package task5;

import org.junit.Before;
import task5.config.Config;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ActiveProfiles(value = "ru")
@ContextConfiguration(classes = {Config.class})
public class DateFormatterWithRuProfileTest {

    @Autowired
    private ApplicationContext applicationContext;
    private DateFormatter dateFormatter;

    @Before
    public void setUp() {
        SimpleDateFormat localeDateFormat = applicationContext.getBean("localeDateFormat", SimpleDateFormat.class);
        SimpleDateFormat isoDateFormat = applicationContext.getBean("isoDateFormat", SimpleDateFormat.class);
        dateFormatter = new DateFormatter(localeDateFormat, isoDateFormat);
    }
    @Test
    public void testGetDateStringAccordingToTodayCommand() {
        Optional<String> dateStringOptional = dateFormatter.getDateStringAccordingTo("today");

        assertTrue(dateStringOptional.isPresent());
        assertTrue(dateStringOptional.get().matches("\\p{L}+, \\d{2} \\p{L}+, \\d{4}"));
    }

    @Test
    public void testGetDateStringAccordingToTodayIsoCommand() {
        Optional<String> dateStringOptional = dateFormatter.getDateStringAccordingTo("today-iso");

        assertTrue(dateStringOptional.isPresent());
        assertTrue(dateStringOptional.get().matches("\\d{4}-\\d{2}-\\d{2}"));
    }
}