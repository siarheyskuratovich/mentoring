package task6.util;

import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.cglib.proxy.Enhancer;
import task6.metric.MethodMetricHolder;
import testclass.ClassWithTimedMethods;
import testclass.TimedClass;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ProximatorTest {

    private final MethodMetricHolder methodMetricHolder = Mockito.spy(new MethodMetricHolder());

    private final TimeUtil timeUtil = new TimeUtil();

    private final Proximator proximator = new Proximator(methodMetricHolder, timeUtil);

    @After
    public void resetMock() {
        reset(methodMetricHolder);
    }

    @Test
    public void makeProxyFromClassWithTimedMethods() {
        ClassWithTimedMethods classWithTimedMethods = new ClassWithTimedMethods();
        classWithTimedMethods = (ClassWithTimedMethods) proximator.makeProxyFrom(classWithTimedMethods);

        assertTrue(Enhancer.isEnhanced(classWithTimedMethods.getClass()));

        classWithTimedMethods.timedMethod();
        verify(methodMetricHolder).addMetric(any(), any());
        reset(methodMetricHolder);

        classWithTimedMethods.notTimedMethod();
        verify(methodMetricHolder, Mockito.never()).addMetric(any(), any());
    }

    @Test
    public void makeProxyFromTimedClass() {
        TimedClass classWithTimedMethods = new TimedClass();
        classWithTimedMethods = (TimedClass) proximator.makeProxyFrom(classWithTimedMethods);

        assertTrue(Enhancer.isEnhanced(classWithTimedMethods.getClass()));

        classWithTimedMethods.timedMethod();
        verify(methodMetricHolder).addMetric(any(), any());
    }
}