package task6.util;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class TimeUtilTest {

    private static final int ONE_MILLI_IN_NANOS = 1000000;

    @Test
    public void getDuration() {
        TimeUtil timeUtil = new TimeUtil();

        LocalDateTime from = LocalDateTime.now();

        for (int i = 0; i < 100; i++) {
            int duration = timeUtil.getDurationInMillis(from, from.plusNanos(ONE_MILLI_IN_NANOS * i));
            assertEquals(i, duration);
        }
    }
}