package task6.metric.provider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import task6.metric.MethodMetricStat;
import task6.util.TimeUtil;
import testclass.ClassWithTimedMethods;
import testclass.TimedClass;
import testclass.config.TestConfig;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class MetricStatProviderImplTest {

    @Autowired
    private ClassWithTimedMethods classWithTimedMethods;
    @Autowired
    private TimedClass timedClass;
    @Autowired
    private MetricStatProvider metricStatProvider;
    @Autowired
    private TimeUtil timeUtil;

    private LocalDateTime startOfFirstPeriod;
    private LocalDateTime endOfFirstPeriod;

    private LocalDateTime startOfSecondPeriod;
    private LocalDateTime endOfSecondPeriod;

    @Before
    public void before() {
        runMethodsOfFirstPeriod();
        runMethodsOfSecondPeriod();
    }

    @Test
    public void getTotalStatForFirstPeriod() {
        List<MethodMetricStat> methodMetricStats = metricStatProvider.getTotalStatForPeriod(startOfFirstPeriod, endOfFirstPeriod);

        assertEquals(4, methodMetricStats.size());

        assertFalse(methodMetricStats.stream()
                .map(MethodMetricStat::getMethodName)
                .anyMatch("ClassWithTimedMethods.notTimedMethod"::equals));

        assertEquals(3, methodMetricStats.stream()
                .filter(stat -> "ClassWithTimedMethods.timedMethod2".equals(stat.getMethodName()))
                .findAny().orElseThrow().getInvocationsCount().intValue());

        assertEquals(3, methodMetricStats.stream()
                .filter(stat -> "ClassWithTimedMethods.timedMethod".equals(stat.getMethodName()))
                .findAny().orElseThrow().getInvocationsCount().intValue());


        assertEquals(0, methodMetricStats.stream()
                .filter(stat -> "TimedClass.timedMethod2".equals(stat.getMethodName()))
                .findAny().orElseThrow().getInvocationsCount().intValue());

        assertEquals(0, methodMetricStats.stream()
                .filter(stat -> "TimedClass.timedMethod".equals(stat.getMethodName()))
                .findAny().orElseThrow().getInvocationsCount().intValue());

        for (MethodMetricStat methodMetricStat : methodMetricStats) {
            System.out.println(methodMetricStat);
        }
    }

    @Test
    public void getTotalStatByMethodForFirstPeriod() {
        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod("ClassWithTimedMethods.timedMethod", startOfFirstPeriod, endOfFirstPeriod);

        assertEquals("ClassWithTimedMethods.timedMethod", stat.getMethodName());
        assertEquals(3, stat.getInvocationsCount().intValue());

        int periodDuration = timeUtil.getDurationInMillis(startOfFirstPeriod, endOfFirstPeriod);
        assertTrue(stat.getMaxTime() <= periodDuration);
        assertTrue(stat.getMinTime() <= periodDuration);
        assertTrue(stat.getAverageTime() <= periodDuration);
    }

    @Test
    public void getTotalStatForFirstPeriodForNotTimedMethod() {
        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod("ClassWithTimedMethods.notTimedMethod", startOfFirstPeriod, endOfFirstPeriod);

        assertEquals("ClassWithTimedMethods.notTimedMethod", stat.getMethodName());
        assertEquals(0, stat.getInvocationsCount().intValue());

        assertEquals(0, stat.getMaxTime().intValue());
        assertEquals(0, stat.getMinTime().intValue());
        assertEquals(0, stat.getAverageTime().intValue());
    }

    @Test
    public void getTotalStatByMethodForNotInvokingPeriod() {
        LocalDateTime anotherFrom = startOfFirstPeriod.minusMinutes(10);
        LocalDateTime anotherTo = endOfFirstPeriod.minusMinutes(5);

        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod("ClassWithTimedMethods.timedMethod", anotherFrom, anotherTo);

        assertEquals("ClassWithTimedMethods.timedMethod", stat.getMethodName());
        assertEquals(0, stat.getInvocationsCount().intValue());

        assertEquals(0, stat.getMaxTime().intValue());
        assertEquals(0, stat.getMinTime().intValue());
        assertEquals(0, stat.getAverageTime().intValue());
    }

    @Test
    public void getTotalStatForPeriodForTimedClass() {
        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod("TimedClass.timedMethod", startOfSecondPeriod, endOfSecondPeriod);

        assertEquals("TimedClass.timedMethod", stat.getMethodName());
        assertEquals(3, stat.getInvocationsCount().intValue());

        int periodDuration = timeUtil.getDurationInMillis(startOfSecondPeriod, endOfSecondPeriod);
        assertTrue(stat.getMaxTime() <= periodDuration);
        assertTrue(stat.getMinTime() <= periodDuration);
        assertTrue(stat.getAverageTime() <= periodDuration);
    }

    @Test
    public void getTotalStatByNullMethod() {
        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod(null, startOfFirstPeriod, endOfFirstPeriod);

        assertNull(stat.getMethodName());
        assertEquals(0, stat.getInvocationsCount().intValue());
        assertEquals(0, stat.getMaxTime().intValue());
        assertEquals(0, stat.getMinTime().intValue());
        assertEquals(0, stat.getAverageTime().intValue());
    }

    @Test
    public void getTotalStatByMethodForNullPeriod() {
        MethodMetricStat stat = metricStatProvider
                .getTotalStatByMethodForPeriod("ClassWithTimedMethods.timedMethod", null, null);

        assertEquals("ClassWithTimedMethods.timedMethod", stat.getMethodName());
        assertEquals(0, stat.getInvocationsCount().intValue());
        assertEquals(0, stat.getMaxTime().intValue());
        assertEquals(0, stat.getMinTime().intValue());
        assertEquals(0, stat.getAverageTime().intValue());
    }

    private void runMethodsOfFirstPeriod() {
        startOfFirstPeriod = LocalDateTime.now();

        classWithTimedMethods.timedMethod();
        classWithTimedMethods.timedMethod();
        classWithTimedMethods.timedMethod();

        classWithTimedMethods.timedMethod2();
        classWithTimedMethods.timedMethod2();
        classWithTimedMethods.timedMethod2();

        classWithTimedMethods.notTimedMethod();
        classWithTimedMethods.notTimedMethod();
        classWithTimedMethods.notTimedMethod();

        endOfFirstPeriod = LocalDateTime.now();

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        classWithTimedMethods.timedMethod2();
    }

    private void runMethodsOfSecondPeriod() {
        startOfSecondPeriod = LocalDateTime.now();

        timedClass.timedMethod();
        timedClass.timedMethod();
        timedClass.timedMethod();

        timedClass.timedMethod2();
        timedClass.timedMethod2();
        timedClass.timedMethod2();

        endOfSecondPeriod = LocalDateTime.now();

        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        timedClass.timedMethod2();
    }
}