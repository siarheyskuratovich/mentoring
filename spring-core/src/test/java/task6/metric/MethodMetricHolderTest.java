package task6.metric;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class MethodMetricHolderTest {
    private final MethodMetricHolder methodMetricHolder = new MethodMetricHolder();

    @Before
    public void fill() {
        methodMetricHolder.addMetric("method1", new MethodMetric(LocalDateTime.now(), 1));

        methodMetricHolder.addMetric("method2", new MethodMetric(LocalDateTime.now(), 2));
        methodMetricHolder.addMetric("method2", new MethodMetric(LocalDateTime.now(), 2));

        methodMetricHolder.addMetric("method3", new MethodMetric(LocalDateTime.now(), 3));
        methodMetricHolder.addMetric("method3", new MethodMetric(LocalDateTime.now(), 3));
        methodMetricHolder.addMetric("method3", new MethodMetric(LocalDateTime.now(), 3));
    }

    @Test
    public void getMethodNameSet() {
        Set<String> methodNames = methodMetricHolder.getMethodNameSet();

        assertEquals(3, methodNames.size());
        assertTrue(methodNames.contains("method1"));
        assertTrue(methodNames.contains("method2"));
        assertTrue(methodNames.contains("method3"));
    }

    @Test
    public void getMetricsByMethod() {
        List<MethodMetric> metricsOfNullMethodName = methodMetricHolder.getMetricsByMethod(null);

        assertNotNull(metricsOfNullMethodName);
        assertEquals(0, metricsOfNullMethodName.size());

        List<MethodMetric> metricsOfMethod2 = methodMetricHolder.getMetricsByMethod("method2");

        assertNotNull(metricsOfNullMethodName);
        assertEquals(2, metricsOfMethod2.size());
    }

    @Test
    public void addMetric() {
        assertTrue(methodMetricHolder.getMethodNameSet().contains("method1"));
        assertTrue(methodMetricHolder.getMethodNameSet().contains("method2"));
        assertTrue(methodMetricHolder.getMethodNameSet().contains("method3"));

        assertNotNull(methodMetricHolder.getMetricsByMethod("method1"));
        assertNotNull(methodMetricHolder.getMetricsByMethod("method2"));
        assertNotNull(methodMetricHolder.getMetricsByMethod("method3"));

        assertEquals(1, methodMetricHolder.getMetricsByMethod("method1").size());
        assertEquals(2, methodMetricHolder.getMetricsByMethod("method2").size());
        assertEquals(3, methodMetricHolder.getMetricsByMethod("method3").size());
    }
}