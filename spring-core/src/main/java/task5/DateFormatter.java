package task5;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class DateFormatter {

    private final SimpleDateFormat localeDateFormat;
    private final SimpleDateFormat isoDateFormat;

    public DateFormatter(SimpleDateFormat localeDateFormat, SimpleDateFormat isoDateFormat) {
        this.localeDateFormat = localeDateFormat;
        this.isoDateFormat = isoDateFormat;
    }

    public Optional<String> getDateStringAccordingTo(String command) {
        switch (command) {
            case "today":
                String today = localeDateFormat.format(new Date());
                return Optional.of(today);

            case "today-iso":
                String todayIso = isoDateFormat.format(new Date());
                return Optional.of(todayIso);

            default:
                return Optional.empty();
        }
    }
}