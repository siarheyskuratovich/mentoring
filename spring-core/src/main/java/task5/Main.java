package task5;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        var ctx = new AnnotationConfigApplicationContext("task5.config");

        SimpleDateFormat localeDateFormat = ctx.getBean("localeDateFormat", SimpleDateFormat.class);
        SimpleDateFormat isoDateFormat = ctx.getBean("isoDateFormat", SimpleDateFormat.class);

        var dateFormatter = new DateFormatter(localeDateFormat, isoDateFormat);

        var scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Enter command \"today\" or \"today-iso\" or \"exit\":");
            String answer = scanner.nextLine();

            if ("exit".equals(answer)) {
                return;
            }

            Optional<String> dateStringOptional = dateFormatter.getDateStringAccordingTo(answer);

            dateStringOptional
                    .ifPresentOrElse(System.out::println, () -> System.out.println("Wrong command, try again."));

        }
    }
}
