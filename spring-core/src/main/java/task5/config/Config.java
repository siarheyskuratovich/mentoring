package task5.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import java.text.SimpleDateFormat;
import java.util.Locale;

@Configuration
public class Config {

    @Bean(name = "localeDateFormat")
    @Profile("ru")
    @Scope("prototype")
    public SimpleDateFormat getDateFormatAccordingToRuLocale() {
        return new SimpleDateFormat("EEEE, dd MMMM, yyyy", new Locale("ru"));
    }

    @Bean(name = "localeDateFormat")
    @Profile({"en", "default"})
    @Scope("prototype")
    public SimpleDateFormat getDateFormatAccordingToEnLocale() {
        return new SimpleDateFormat("EEEE, MMMM, dd, yyyy", Locale.ENGLISH);
    }

    @Bean(name = "isoDateFormat")
    @Profile({"en", "ru", "default"})
    @Scope("prototype")
    public SimpleDateFormat getIsoDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
}