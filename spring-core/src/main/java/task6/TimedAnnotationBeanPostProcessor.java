package task6;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import task6.util.Proximator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class TimedAnnotationBeanPostProcessor implements BeanPostProcessor {

    private final Proximator proximator;

    private final Map<String, Class<?>> beanNameAndClassMap = new HashMap<>();


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Timed.class) || containsAnnotatedMethods(beanClass)) {
            beanNameAndClassMap.put(beanName, beanClass);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = beanNameAndClassMap.get(beanName);

        if (beanClass != null) {
            return proximator.makeProxyFrom(bean);
        }

        return bean;
    }

    private boolean containsAnnotatedMethods(Class<?> beanClass) {
        return Arrays.stream(beanClass.getDeclaredMethods())
                .anyMatch(method -> method.isAnnotationPresent(Timed.class));
    }
}
