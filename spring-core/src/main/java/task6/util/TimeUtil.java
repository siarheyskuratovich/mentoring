package task6.util;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Service
public class TimeUtil {
    public Integer getDurationInMillis(LocalDateTime from, LocalDateTime to) {
        return Math.toIntExact(localDateTimeToMillis(to) - localDateTimeToMillis(from));
    }

    private long localDateTimeToMillis(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
