package task6.util;

import lombok.RequiredArgsConstructor;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.stereotype.Service;
import task6.metric.MethodMetric;
import task6.metric.MethodMetricHolder;
import task6.Timed;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class Proximator {

    private final MethodMetricHolder methodMetricHolder;
    private final TimeUtil timeUtil;

    public Object makeProxyFrom(Object object) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(object.getClass());
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {

            Class<?> declaringClass = method.getDeclaringClass();
            if (declaringClass.isAnnotationPresent(Timed.class) || method.isAnnotationPresent(Timed.class)) {

                LocalDateTime from = LocalDateTime.now();
                Object result = proxy.invokeSuper(obj, args);
                LocalDateTime to = LocalDateTime.now();

                Integer duration = timeUtil.getDurationInMillis(from, to);

                String methodName = method.getDeclaringClass().getSimpleName() + "." + method.getName();

                methodMetricHolder
                        .addMetric(methodName, new MethodMetric(from, duration));

                return result;
            }

            return proxy.invokeSuper(obj, args);
        });
        return enhancer.create();
    }
}


