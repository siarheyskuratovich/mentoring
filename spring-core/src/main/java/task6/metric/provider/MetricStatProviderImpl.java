package task6.metric.provider;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import task6.metric.MethodMetricHolder;
import task6.metric.MethodMetric;
import task6.metric.MethodMetricStat;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MetricStatProviderImpl implements MetricStatProvider {
    private final MethodMetricHolder methodMetricHolder;

    @Override
    public List<MethodMetricStat> getTotalStatForPeriod(LocalDateTime from, LocalDateTime to) {
        Set<String> methodNameSet = methodMetricHolder.getMethodNameSet();

        List<MethodMetricStat> methodMetricStatList = new ArrayList<>();

        for (String methodName : methodNameSet) {
            methodMetricStatList.add(getTotalStatByMethodForPeriod(methodName, from, to));
        }

        return methodMetricStatList;
    }

    @Override
    public MethodMetricStat getTotalStatByMethodForPeriod(String method, LocalDateTime from, LocalDateTime to) {
        LocalDateTime fromClosure = Optional.ofNullable(from).orElse(LocalDateTime.MAX);
        LocalDateTime toClosure = Optional.ofNullable(to).orElse(LocalDateTime.MIN);

        List<MethodMetric> filteredByPeriodMetrics = methodMetricHolder.getMetricsByMethod(method).stream()
                .filter(metric -> !metric.getInvocationTime().isBefore(fromClosure))
                .filter(metric -> !metric.getInvocationTime().isAfter(toClosure))
                .collect(Collectors.toList());

        int invocationsCount = filteredByPeriodMetrics.size();

        Integer minTime = filteredByPeriodMetrics.stream()
                .mapToInt(MethodMetric::getDuration)
                .min()
                .orElse(0);

        Integer maxTime = filteredByPeriodMetrics.stream()
                .mapToInt(MethodMetric::getDuration)
                .max()
                .orElse(0);

        Integer averageTime = (int) filteredByPeriodMetrics.stream()
                .mapToInt(MethodMetric::getDuration)
                .average()
                .orElse(0);


        return MethodMetricStat.builder()
                .methodName(method)
                .invocationsCount(invocationsCount)
                .minTime(minTime)
                .maxTime(maxTime)
                .averageTime(averageTime)
                .build();
    }
}
