package task6.metric.provider;

import task6.metric.MethodMetricStat;

import java.time.LocalDateTime;
import java.util.List;

public interface MetricStatProvider {

    List<MethodMetricStat> getTotalStatForPeriod (LocalDateTime from, LocalDateTime to);

    MethodMetricStat getTotalStatByMethodForPeriod(String method, LocalDateTime from, LocalDateTime to);
}
