package task6.metric;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MethodMetricHolder {
    private final Map<String, List<MethodMetric>> metrics = new ConcurrentHashMap<>();

    public Set<String> getMethodNameSet() {
        return metrics.keySet();
    }

    public List<MethodMetric> getMetricsByMethod(String methodName) {
        methodName = Optional.ofNullable(methodName).orElse("");
        return metrics.getOrDefault(methodName, Collections.emptyList());
    }

    public void addMetric(String methodName, MethodMetric metric) {
        metrics.computeIfAbsent(methodName, k -> new ArrayList<>());
        metrics.get(methodName).add(metric);
    }
}
