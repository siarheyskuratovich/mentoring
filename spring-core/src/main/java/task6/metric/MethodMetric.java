package task6.metric;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
public class MethodMetric {

    private final LocalDateTime invocationTime;

    private final Integer duration;
}
