package task6.metric;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
@Getter
public class MethodMetricStat {

    private String methodName;

    private Integer invocationsCount;

    private Integer minTime;

    private Integer averageTime;

    private Integer maxTime;
}
