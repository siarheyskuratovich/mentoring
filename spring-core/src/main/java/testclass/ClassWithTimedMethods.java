package testclass;

import org.springframework.stereotype.Component;
import task6.Timed;

@Component
public class ClassWithTimedMethods {
    @Timed
    public void timedMethod() {
        System.out.println("timedMethod");
    }

    @Timed
    public void timedMethod2() {
        System.out.println("timedMethod2");
    }

    public void notTimedMethod() {
        System.out.println("notTimedMethod");
    }
}
