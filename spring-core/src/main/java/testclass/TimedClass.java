package testclass;

import org.springframework.stereotype.Component;
import task6.Timed;

@Component
@Timed
public class TimedClass {
    public void timedMethod() {
        System.out.println("timedMethod");
    }

    public void timedMethod2() {
        System.out.println("timedMethod2");
    }
}
