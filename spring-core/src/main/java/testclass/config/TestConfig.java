package testclass.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import task6.config.Config;

@Configuration
@ComponentScan("testclass")
@Import(Config.class)
public class TestConfig {
}
