package task7.var1;

import java.util.concurrent.CompletableFuture;

public class LegVar1 implements Runnable {

    private static final Object MONITOR = new Object();

    private final String name;

    public LegVar1(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (MONITOR) {
                System.out.println(name);
                MONITOR.notifyAll();
                try {
                    MONITOR.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public static void main(String[] args) {
        CompletableFuture.allOf(
                        CompletableFuture.runAsync(new LegVar1("left")),
                        CompletableFuture.runAsync(new LegVar1("right")))
                .join();
    }
}
