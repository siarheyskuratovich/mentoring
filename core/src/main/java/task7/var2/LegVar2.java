package task7.var2;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LegVar2 implements Runnable {

    private final String name;

    public LegVar2(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println(name);
    }

    public static void main(String[] args) {
        LegVar2 leftLegTaskVar1 = new LegVar2("left");
        LegVar2 rightLegTaskVar1 = new LegVar2("right");

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        while (true) {
            CompletableFuture
                    .runAsync(leftLegTaskVar1, executorService)
                    .thenRunAsync(rightLegTaskVar1, executorService)
                    .join();
        }
    }
}