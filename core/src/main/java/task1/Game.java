package task1;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import task1.exception.IncorrectPlayerStringFormatException;
import task1.exception.EmptyPlayerListException;


public class Game {
    private static final Map<String, ScoreIndex> nameAndScoreIndexMap = new HashMap<>();

    public static String showWinner(List<String> players) throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        if (players == null || players.isEmpty()) {
            throw new EmptyPlayerListException("Players list is empty or null");
        }

        try {
            Map<String, ScoreIndex> nameAndScoreIndexMap = collectToNameAndScoreIndexMap(players);
            return nameAndScoreIndexMap.entrySet().stream()
                    .max(Game::selectWinner)
                    .map(Map.Entry::getKey)
                    .orElse("");
        } finally {
            nameAndScoreIndexMap.clear();
        }
    }

    private static Map<String, ScoreIndex> collectToNameAndScoreIndexMap(List<String> players) throws IncorrectPlayerStringFormatException {

        for (int i = 0; i < players.size(); i++) {
            String playerString = players.get(i);

            if (isPlayerStringFormatIncorrect(playerString)) {
                throw new IncorrectPlayerStringFormatException("Incorrect player string format \"" + playerString + "\"");
            }

            Map.Entry<String, ScoreIndex> nameAndScoreIndexEntry = parsePlayerStringToMapEntry(playerString, i);
            nameAndScoreIndexMap.put(nameAndScoreIndexEntry.getKey(), nameAndScoreIndexEntry.getValue());

        }
        return nameAndScoreIndexMap;
    }

    private static boolean isPlayerStringFormatIncorrect(String player) {
        return player == null || !player.matches("\\D+\\d+");
    }

    private static Map.Entry<String, ScoreIndex> parsePlayerStringToMapEntry(String player, int scoreIndex) throws IncorrectPlayerStringFormatException {
        Pattern digitPattern = Pattern.compile("\\d");
        Matcher digitMatcher = digitPattern.matcher(player);

        if (!digitMatcher.find()) {
            throw  new IncorrectPlayerStringFormatException("Incorrect player string format \"" + player + "\"");
        }

        String playerName = player.substring(0, digitMatcher.start()).trim();
        int playerScore = Integer.parseInt(player.substring(digitMatcher.start()));

        int lastPlayerScore = Optional.ofNullable(nameAndScoreIndexMap.get(playerName))
                .map(scoreAndIndex -> scoreAndIndex.score)
                .orElse(0);

        return Map.entry(playerName, new ScoreIndex(lastPlayerScore + playerScore, scoreIndex));
    }

    private static int selectWinner(Map.Entry<String, ScoreIndex> o1, Map.Entry<String, ScoreIndex> o2) {
        int result = Integer.compare(o1.getValue().score, o2.getValue().score);
        if (result == 0) {
            return Integer.compare(o2.getValue().index, o1.getValue().index);
        }
        return result;
    }

    private static class ScoreIndex {
        int score;
        int index;

        public ScoreIndex(int score, int index) {
            this.score = score;
            this.index = index;
        }
    }
}