package task1.exception;

public class EmptyPlayerListException extends Exception{
    public EmptyPlayerListException(String message) {
        super(message);
    }
}