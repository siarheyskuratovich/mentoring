package task1.exception;

public class IncorrectPlayerStringFormatException extends Exception{
    public IncorrectPlayerStringFormatException(String message) {
        super(message);
    }
}