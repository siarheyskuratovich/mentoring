package task4;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.util.*;
import java.util.stream.Collectors;

public class SearchServiceImpl implements SearchService {
    private final List<User> usersWeAreLookingFor;
    private final List<Vertex> allVisitedVertexes;

    public SearchServiceImpl() {
        usersWeAreLookingFor = new ArrayList<>();
        allVisitedVertexes = new ArrayList<>();
    }

    @Override
    public List<User> searchForFriendsInDepth(User me, String name) {
        resetState();
        Stack<Vertex> vertexStack = new Stack<>();

        Vertex currentVertex = new Vertex(me, true);
        vertexStack.push(currentVertex);
        allVisitedVertexes.add(currentVertex);

        while (!vertexStack.isEmpty()) {
            currentVertex = vertexStack.peek();

            Optional<Vertex> optionalNotVisitedFriendVertex = currentVertex.user.getFriends().stream()
                    .filter(this::isNotVisited)
                    .findAny()
                    .map(friend -> new Vertex(friend, false));

            if (optionalNotVisitedFriendVertex.isEmpty()) {
                vertexStack.pop();
                continue;
            }

            Vertex notVisitedFriendVertex = optionalNotVisitedFriendVertex.get();

            if (notVisitedFriendVertex.user.getName().equals(name)) {
                usersWeAreLookingFor.add(notVisitedFriendVertex.user);
            }

            currentVertex = new Vertex(notVisitedFriendVertex.user, true);
            vertexStack.push(currentVertex);
            allVisitedVertexes.add(currentVertex);
        }
        return usersWeAreLookingFor;
    }

    @Override
    public List<User> searchForFriendsInWidth(User me, String name) {
        resetState();
        Queue<Vertex> vertexQueue = new LinkedList<>();

        Vertex currentVertex = new Vertex(me, true);
        vertexQueue.add(currentVertex);
        allVisitedVertexes.add(currentVertex);

        while (!vertexQueue.isEmpty()) {
            currentVertex = vertexQueue.poll();

            List<Vertex> notVisitedFriendVertexes = currentVertex.user.getFriends().stream()
                    .filter(this::isNotVisited)
                    .map(friend -> new Vertex(friend, false))
                    .collect(Collectors.toList());

            if (notVisitedFriendVertexes.isEmpty()) {
                continue;
            }

            for (Vertex friendVertex : notVisitedFriendVertexes) {
                friendVertex.isVisited = true;
                allVisitedVertexes.add(friendVertex);

                if (friendVertex.user.getName().equals(name)) {
                    usersWeAreLookingFor.add(friendVertex.user);
                }

                vertexQueue.add(friendVertex);
            }
        }
        return usersWeAreLookingFor;
    }

    private void resetState() {
        usersWeAreLookingFor.clear();
        allVisitedVertexes.clear();
    }

    private boolean isNotVisited(User user) {
        return allVisitedVertexes.stream().noneMatch(vertex -> user.equals(vertex.user));
    }

    @EqualsAndHashCode
    @AllArgsConstructor
    private static class Vertex {
        public final User user;
        public boolean isVisited;
    }
}