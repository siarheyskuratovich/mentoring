package task4;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@EqualsAndHashCode
@Getter
public class User {

    private static int usersNumber = 0;
    private final long id;
    private final String name;
    @Setter
    private List<User> friends;

    public User(String name) {
        this.name = name;
        this.id = ++usersNumber;
    }
}