package task2;

public class Post implements Comparable<Post> {
    private int likeQuantity;

    public Post(int likeQuantity) {
        this.likeQuantity = likeQuantity;
    }

    public int getLikeQuantity() {
        return likeQuantity;
    }

    @Override
    public int compareTo(Post post) {
        return Integer.compare(this.likeQuantity, post.getLikeQuantity());
    }

    @Override
    public String toString() {
        return String.valueOf(likeQuantity);
    }
}