package task2;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

public class PostExtractor {
    private static final int TOP = 10;

    public static List<Post> getTop10(List<Post> posts) {
        Queue<Post> top10 = new PriorityQueue<>(TOP + 1);

        for (int i = 0; i < TOP; i++) {
            top10.add(posts.get(i));
        }

        for (int i = TOP; i < posts.size(); i++) {
            top10.add(posts.get(i));
            top10.poll();
        }

        return top10.stream().sorted().collect(Collectors.toList());
    }
}