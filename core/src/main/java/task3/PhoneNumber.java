package task3;

public class PhoneNumber {
    private String number;
    private Type type;

    public PhoneNumber(String number, Type type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        MOBILE, HOME
    }
}