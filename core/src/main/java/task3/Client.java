package task3;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Client {
    private UUID id;
    private String name;
    private int age;
    private List<PhoneNumber> phoneNumbers;

    public Client(UUID id, String name, int age, List<PhoneNumber> phoneNumbers) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.phoneNumbers = phoneNumbers;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }
}