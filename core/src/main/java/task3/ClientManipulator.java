package task3;

import java.util.*;
import java.util.stream.Collectors;

public class ClientManipulator {

    public static int calculateAgeSumFor(String name, Client[] clients) {
        return Arrays.stream(clients)
                .filter(client -> client.getName().equals(name))
                .mapToInt(Client::getAge)
                .sum();
    }

    public static Set<String> getNameSet(Client[] clients) {
        return Arrays.stream(clients)
                .map(Client::getName)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static boolean areThereAnyClientsOlderThan(int age, List<Client> clients) {
        return clients.stream()
                .anyMatch(client -> client.getAge() > age);
    }

    public static Map<UUID, String> toIdNameMap(Client[] clients) {
        return Arrays.stream(clients)
                .collect(Collectors.toMap(Client::getId, Client::getName, (left, right) -> left, LinkedHashMap::new));
    }

    public static Map<Integer, List<Client>> toAgeAndCollectionByAgeMap(Client[] clients) {
        return Arrays.stream(clients).collect(Collectors.groupingBy(Client::getAge, Collectors.toList()));
    }

    public static String buildAllClientPhoneNumbersString(Client[] clients) {
        return Arrays.stream(clients)
                .map(client -> Optional.ofNullable(client.getPhoneNumbers())
                        .orElse(Collections.emptyList()))
                .map(phoneNumbers -> phoneNumbers.stream()
                        .map(PhoneNumber::getNumber)
                        .collect(Collectors.joining(", ")))
                .collect(Collectors.joining(", "));
    }

    public static Optional<Client> getTheOldestClientUsingHomePhone(Client[] clients) {
        return Arrays.stream(clients)
                .filter(client -> Optional.ofNullable(client.getPhoneNumbers())
                        .orElse(Collections.emptyList())
                        .stream()
                        .map(PhoneNumber::getType)
                        .anyMatch(type -> type == PhoneNumber.Type.HOME))
                .max(Comparator.comparing(Client::getAge));
    }
}