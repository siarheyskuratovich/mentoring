package task1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import task1.exception.IncorrectPlayerStringFormatException;
import task1.exception.EmptyPlayerListException;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test
    public void selectTheWinnerTrivial() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        List<String> players = List.of("Pasha3", "Sasha2", "Sergey 5");

        assertEquals("Sergey", Game.showWinner(players));
    }

    @Test
    public void selectTheWinnerIfPlayerScoredMultipleTimes() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        List<String> players = List.of("Pasha3", "Sasha2", "Pasha3", "Sergey5");

        assertEquals("Pasha", Game.showWinner(players));
    }

    @Test
    public void selectTheWinnerIfPlayerScoredMultipleTimes2() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        List<String> players = List.of("Pasha3", "Sasha2", "Pasha2", "Sergey5", "Sergey1", "Pasha1");
        assertEquals("Sergey", Game.showWinner(players));

        List<String> playersList1 = List.of("Pasha3", "Sasha2", "Pasha3", "Sergey5", "Sergey1");
        assertEquals("Pasha", Game.showWinner(playersList1));
    }

    @Test(expected = IncorrectPlayerStringFormatException.class)
    public void selectTheWinnerIfListOfPlayersIncorrect() throws Exception {
        List<String> playersList1 = List.of("Pas3ha3", "Sas2ha2", "Pas4ha3", "Ser5gey5", "Serg7ey1");

        Game.showWinner(playersList1);
    }

    @Test(expected = EmptyPlayerListException.class)
    public void selectTheWinnerIfListIsEmpty() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        List<String> playersList1 = Collections.emptyList();

        Game.showWinner(playersList1);
    }

    @Test(expected = EmptyPlayerListException.class)
    public void selectTheWinnerIfListIsNull() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        Game.showWinner(null);
    }

    @Test()
    public void selectTheWinnerIfListContains1Player() throws Exception {
        List<String> playersList1 = List.of("Pasha3");

        Game.showWinner(playersList1);
        assertEquals("Pasha", Game.showWinner(playersList1));
    }

    @Test(expected = IncorrectPlayerStringFormatException.class)
    public void selectTheWinnerIfListOfNulls() throws IncorrectPlayerStringFormatException, EmptyPlayerListException {
        List<String> strings = new ArrayList<>();
        strings.add(null);
        strings.add(null);
        strings.add(null);
        Game.showWinner(strings);
    }
}