package task3;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class ClientManipulatorTest {

    @Test
    public void testCalculateAgeSumFor() {
        Client[] clients = new Client[4];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        assertEquals(43, ClientManipulator.calculateAgeSumFor("Sergey", clients));
    }

    @Test
    public void testGetNameSet() {
        Client[] clients = new Client[5];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Kirill", 16, null);

        Set<String> names = ClientManipulator.getNameSet(clients);
        assertTrue(names.contains("Sergey"));
        assertTrue(names.contains("Pasha"));
        assertTrue(names.contains("Kirill"));
        assertEquals(3, names.size());

        List<String> nameList = new ArrayList<>(names);
        assertEquals("Sergey", nameList.get(0));
        assertEquals("Pasha", nameList.get(1));
        assertEquals("Kirill", nameList.get(2));
    }

    @Test
    public void testAreThereAnyClientsOlderThan() {
        Client[] clients = new Client[5];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Kirill", 16, null);

        List<Client> clientList = Arrays.stream(clients).collect(Collectors.toList());

        assertFalse(ClientManipulator.areThereAnyClientsOlderThan(17, clientList));
        assertTrue(ClientManipulator.areThereAnyClientsOlderThan(12, clientList));
    }

    @Test
    public void testToIdNameMap() {
        Client[] clients = new Client[5];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Kirill", 16, null);

        List<Map.Entry<UUID, String>> entryList = new ArrayList<>(ClientManipulator.toIdNameMap(clients).entrySet());
        for (int i = 0; i < clients.length; i++) {
            assertEquals(clients[i].getId(), entryList.get(i).getKey());
        }

        for (int i = 0; i < clients.length; i++) {
            assertEquals(clients[i].getName(), entryList.get(i).getValue());
        }
    }

    @Test
    public void testToAgeAndCollectionByAgeMap() {
        Client[] clients = new Client[5];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Kirill", 16, null);


        Map<Integer, List<Client>> resultMap = ClientManipulator.toAgeAndCollectionByAgeMap(clients);
        assertEquals(3, resultMap.size());

        assertTrue(resultMap.containsKey(12));
        assertTrue(resultMap.containsKey(15));
        assertTrue(resultMap.containsKey(16));
        assertFalse(resultMap.containsKey(20));

        assertEquals(2, resultMap.get(12).size());
        assertEquals(1, resultMap.get(15).size());
        assertEquals(2, resultMap.get(16).size());

        assertTrue(resultMap.get(12).stream().map(Client::getAge).allMatch(age -> age == 12));
        assertTrue(resultMap.get(15).stream().map(Client::getAge).allMatch(age -> age == 15));
        assertTrue(resultMap.get(16).stream().map(Client::getAge).allMatch(age -> age == 16));
    }

    @Test
    public void testBuildAllClientPhoneNumbersString() {

        Client[] clients = new Client[5];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Kirill", 16, null);

        for (Client client : clients) {
            List<PhoneNumber> phoneNumbers = new ArrayList<>();
            phoneNumbers.add(new PhoneNumber(String.valueOf((int) (Math.random() * 10000000)), PhoneNumber.Type.MOBILE));
            phoneNumbers.add(new PhoneNumber(String.valueOf((int) (Math.random() * 100000)), PhoneNumber.Type.HOME));
            client.setPhoneNumbers(phoneNumbers);
        }

        clients[3].setPhoneNumbers(Collections.emptyList());
        clients[4].setPhoneNumbers(null);

        String phoneNumbersString = ClientManipulator.buildAllClientPhoneNumbersString(clients);
        for (int i = 0; i < 3; i++) {
            Client client = clients[i];
            for (PhoneNumber phoneNumber : client.getPhoneNumbers()) {
                assertTrue(phoneNumbersString.contains(phoneNumber.getNumber()));
            }
        }
    }

    @Test
    public void testGetTheOldestClientUsingHomePhone() {

        Client[] clients = new Client[6];
        clients[0] = new Client(UUID.randomUUID(), "Sergey", 12, null);
        clients[1] = new Client(UUID.randomUUID(), "Pasha", 12, null);
        clients[2] = new Client(UUID.randomUUID(), "Sergey", 15, null);
        clients[3] = new Client(UUID.randomUUID(), "Sergey", 16, null);
        clients[4] = new Client(UUID.randomUUID(), "Alex", 44, null);
        clients[5] = new Client(UUID.randomUUID(), "Kirill", 55, null);

        for (Client client : clients) {
            List<PhoneNumber> phoneNumbers = new ArrayList<>();
            phoneNumbers.add(new PhoneNumber(String.valueOf((int) (Math.random() * 10000000)), PhoneNumber.Type.MOBILE));
            phoneNumbers.add(new PhoneNumber(String.valueOf((int) (Math.random() * 100000)), PhoneNumber.Type.HOME));
            client.setPhoneNumbers(phoneNumbers);
        }

        clients[3].setPhoneNumbers(Collections.emptyList());
        clients[5].setPhoneNumbers(null);

        Optional<Client> oldestClientOptional = ClientManipulator.getTheOldestClientUsingHomePhone(clients);

        assertTrue(oldestClientOptional.isPresent());
        assertEquals("Alex", oldestClientOptional.get().getName());
        assertEquals(44, oldestClientOptional.get().getAge());
    }
}