package task2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PostExtractorTest {

    @Test
    public void testGetTop10() {
        List<Post> posts = new ArrayList<>(10);
        for (int i = 0; i < 1000000; i++) {
            posts.add(new Post((int) (Math.random() * 1000000)));
        }

        List<Post> sorted = PostExtractor.getTop10(posts);
        System.out.println(sorted);

        for (int i = 0; i < sorted.size() - 1; i++) {
            assertTrue(sorted.get(i).getLikeQuantity() <= sorted.get(i + 1).getLikeQuantity());
        }
    }
}